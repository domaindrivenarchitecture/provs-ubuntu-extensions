# Provs-ubuntu

This repo is part of the [provs framework](https://gitlab.com/domaindrivenarchitecture/provs-docs).  
It provides tooling for Ubuntu for provisioning of 
* a desktop workplace

and for provisioning server-software like
* nginx
* certbot (for letsencrypt setup)
* nexus 
* prometheus 
* ...


# Usage

For usage examples it is recommended also to have a look at [provs-scripts](https://gitlab.com/domaindrivenarchitecture/provs-scripts) or at the modules inside this repo.

## Prerequisites

* A **Java Virtual machine** (JVM) is required.

## Setup

* Clone this repo
* Build the jar file by `./gradlew uberJarLatest`

The uberjar is a Java jar-file containing all dependencies incl. Kotlin libs.


## Examples

### Run HelloWorld

Run in the repo's root folder:

`java -jar build/libs/provs-extensions-uber.jar io.provs.ubuntu.extensions.demos.HelloWorldKt`

### Provision a desktop workplace remotely

Run in the repo's root folder:

`java -jar build/libs/provs-extensions-uber.jar io.provs.ubuntu.extensions.workplace.ProvisionWorkplaceKt provisionRemote <ip> <user>`

You'll be prompted for the password of the remote user. 



