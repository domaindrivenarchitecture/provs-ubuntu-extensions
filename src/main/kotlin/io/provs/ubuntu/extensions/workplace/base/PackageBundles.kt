package io.provs.ubuntu.extensions.workplace.base

val OS_ANALYSIS = "lsof strace ncdu iptraf htop iotop iftop"

val ZIP_UTILS = "p7zip-rar p7zip-full rar unrar zip unzip"

val BASH_UTILS = "bash-completion"

val SPELLCHECKING_DE = "hyphen-de hunspell hunspell-de-de"

val OPEN_VPM = "openvpn network-manager-openvpn network-manager-openvpn-gnome"

val OPENCONNECT = "openconnect network-manager-openconnect network-manager-openconnect-gnome"

val VPNC = "vpnc network-manager-vpnc network-manager-vpnc-gnome vpnc-scripts"

val JAVA_JDK = "openjdk-8-jdk openjdk-11-jdk openjdk-14-jdk"